package fr.esiea.unique.valnet.dictionary;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DictionaryTest {

	Dictionary dictionary;
	
	@Before
	public void setUp(){
		dictionary = new Dictionary();
	}

	@Test
	public void testIsWord() {
    	assertTrue(dictionary.isWord("maman"));
        assertFalse(dictionary.isWord("namam"));
    }

}
