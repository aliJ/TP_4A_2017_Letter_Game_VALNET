package fr.esiea.unique.valnet.objects;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PlayerTest {
	
	Player player;
	CommonPot commonPot;
	ArrayList<String> words;
	
	@Before
	public void setUp() throws Exception {
		player = new Player();
		commonPot = new CommonPot();
		words = new ArrayList<String>();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testPickLetter(){
		Character letter = player.pickLetter();
		commonPot.addLetter(letter);
		assertEquals("1 letter", 1, commonPot.getCommonPot().size());
		player.pickLetter();
		commonPot.addLetter(letter);
		assertEquals("2 letters", 2, commonPot.getCommonPot().size());
	}
	
	@Test
	public final void testAddWord(){
		words.add("maman");
		player.addWord("maman");
		assertEquals("words contains [maman]", words, player.getWords());
	}

	@Test
	public final void testDeleteWord(){
		words.add("maman");
		player.addWord("maman");
		player.addWord("tata");
		player.deleteWord("tata");
		assertEquals("words contains [maman]", words, player.getWords());
	}
	
	@Test
	public final void getWordsOfArrayList(){
		
	}
	
	@Test
	public final void testToString(){
		assertEquals("tostring", "player", player.toString());
	}

}
