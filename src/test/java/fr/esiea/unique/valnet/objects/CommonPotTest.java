package fr.esiea.unique.valnet.objects;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CommonPotTest {

	private CommonPot commonPot;
	ArrayList<Character> list;
	
	@Before
	public void setUp(){
		commonPot = new CommonPot();
		list = new ArrayList<Character>();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testAddLetter() {
		list.add('a');
		list.add('z');
		commonPot.addLetter('a');
		commonPot.addLetter('z');
		assertEquals("commonPot contains [A, z]", list, commonPot.getCommonPot());
	}
	
	@Test
	public final void testDeleteLetter(){
		list.add('z');
		commonPot.addLetter('a');
		commonPot.addLetter('z');
		commonPot.deleteLetter('a');
		assertEquals("commonPot contains [z]", list, commonPot.getCommonPot());
	}
	
	@Test
	public final void testDeleteWord(){
		list.add('a');
		commonPot.addLetter('j');
		commonPot.addLetter('o');
		commonPot.addLetter('i');
		commonPot.addLetter('e');
		commonPot.addLetter('a');
		commonPot.deleteWord("joie");
		
		assertEquals("commonPot contains [a]", list, commonPot.getCommonPot());
	}
	
	@Test
	public final void testToString(){
		String expected = "[]";
		assertEquals(expected, commonPot.toString());
	}
}
