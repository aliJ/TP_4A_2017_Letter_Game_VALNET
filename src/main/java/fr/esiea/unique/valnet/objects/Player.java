package fr.esiea.unique.valnet.objects;

import java.util.ArrayList;
import java.util.Random;

public class Player {
	public ArrayList<String> getWords() {
		return words;
	}

	private static int counter = 1;
	public final int idPlayer;
	public ArrayList<String> words;
	
	public Player() {
		words = new ArrayList<String>();
		this.idPlayer = counter++;
	}

	public char pickLetter(){
		Random randParam = new Random();
		char randChar = (char)(randParam.nextInt(26) + 'a');
		return randChar;
	}
	
	public char pickLetter(LetterOccurrenceFrequency letterOccurrenceFrequency){
		return letterOccurrenceFrequency.roll();
	}
	
	public void addWord(String word){
		words.add(word);
	}
	
	public void deleteWord(String word){
		words.remove(word);
	}
	

	public ArrayList<String> getWordsOfArrayList(String word){
		ArrayList<String> arraylist = new ArrayList<String>();
		for(String wordOfArray : words){
			if(word.indexOf(wordOfArray)>=0){
				arraylist.add(wordOfArray);
			}
		}
		return arraylist;
	}

	public String getFirstPossibleWordWithPot(CommonPot pot, String word){
		return getFirstPossibleWordWithPot(pot, word, false);
	}
	
	public String stealFirstPossibleWordWithPot(CommonPot pot, String word){
		return getFirstPossibleWordWithPot(pot, word, true);
	}
	
	private String getFirstPossibleWordWithPot(CommonPot pot, String word, boolean stealWord){
		for(String wordFromPlayer : getWordsOfArrayList(word)){
			String remainingLetters = word.replace(wordFromPlayer, "");
			if(pot.contains(remainingLetters)){
				if (stealWord) {
					deleteWord(wordFromPlayer);
					pot.deleteWord(remainingLetters);
				}
				return wordFromPlayer;
			}
		}
		return null;
	}
	
	public boolean isWordPossibleWithPot(CommonPot pot, String word){
		return getFirstPossibleWordWithPot(pot, word) != null;
	}
	
	
	@Override
	public String toString() {
		return "Player "+ idPlayer;
	}
}
