package fr.esiea.unique.valnet.objects;

import java.util.Map;
import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

public class LetterOccurrenceFrequency {
	private final Random generator = new Random();

	  private final NavigableMap<Integer, Character> table = 
	    new TreeMap<Integer, Character>();

	  private final Integer max;
	  
	  public LetterOccurrenceFrequency(Map<Character, Integer> frequency)
	  {
	    Integer total = 0;
	    for (Map.Entry<Character, Integer> e : frequency.entrySet()) {
	      total += e.getValue();
	      table.put(total, e.getKey());
	    }
	    max = total;
	  }

	  /** 
	   * Choose a random symbol. The choices are weighted by frequency.
	   */ 
	  public Character roll()
	  {
	    Integer key = (int) (generator.nextFloat() * max);
	    return table.higherEntry(key).getValue();
	  }
}
