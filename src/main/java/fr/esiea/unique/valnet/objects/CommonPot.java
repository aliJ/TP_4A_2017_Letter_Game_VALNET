package fr.esiea.unique.valnet.objects;

import java.util.ArrayList;

public class CommonPot {
	
	private ArrayList<Character> commonPot;

	public CommonPot(){
		commonPot = new ArrayList<Character>();
	}
	
	public ArrayList<Character> getCommonPot() {
		return commonPot;
	}
	
	public void addLetter(Character letter){
		commonPot.add(letter);
	}
	
	public void deleteLetter(Character letter){
		commonPot.remove(letter);
	}
	
	public void deleteWord(String word){
		for(int i=0; i < word.length(); i++){
			Character letter = word.charAt(i);
			deleteLetter(letter);
		}
	}
	
	@Override
	public String toString() {
		return commonPot.toString();
	}

	public boolean contains(String word) {
		ArrayList <Character> commonPotCopy = new ArrayList <Character>(commonPot);
		for(Character letter : word.toCharArray()){
			if(!commonPotCopy.contains(letter)){
				return false;
			}else{
				commonPotCopy.remove(letter);
			}
		}
		return true;
	}
}
