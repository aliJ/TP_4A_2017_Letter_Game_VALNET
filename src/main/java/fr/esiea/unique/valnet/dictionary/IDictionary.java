package fr.esiea.unique.valnet.dictionary;

public interface IDictionary {

	boolean isWord(String testedWord);
	
}
