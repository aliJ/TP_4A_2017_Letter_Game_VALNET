package fr.esiea.unique.valnet.dictionary;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import fr.esiea.unique.valnet.objects.LetterOccurrenceFrequency;

public class Dictionary implements IDictionary {
	
	private HashSet<String> words;
	
	public Dictionary(){
		words = File();
	}

	private HashSet<String> File() {
		String file = "src/main/resources/dico.txt";
		HashSet<String> hs = new HashSet<String>();
		BufferedReader br = null;
		try {//Ouvrir File
			InputStream ips = new FileInputStream(file);
			InputStreamReader ipsr = new InputStreamReader(ips);
			br = new BufferedReader(ipsr);			
		} 
		catch (Exception e) {
			System.err.println(e.toString());
		}
		
		if(br != null){ //Lecture File = Remplissage arbre à partir des données du File
			String currentLine;
			try{
				do{
					currentLine = br.readLine();
		            hs.add(currentLine);
				} while (br.ready());
				br.close();
		
			}catch (Exception e){
				System.err.println(e.toString());
			}
		}
		
		return hs;
	}

	@Override
	public boolean isWord(String testedWord) {
		return words.contains(testedWord);
	}
	
	public Map<Character, Integer> getLetterFrequencyMap(){
		Map<Character, Integer> frequency = new HashMap<Character, Integer>();
		for(String word : words){
			for(Character letter : word.toLowerCase().toCharArray()){
				if(letter != '-' && letter != 'ã'){
					frequency.putIfAbsent(letter, 0);
					frequency.put(letter, frequency.get(letter)+1);
				}
			}
			
		}
		return frequency;
	}
}
