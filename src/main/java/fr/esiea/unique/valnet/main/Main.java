package fr.esiea.unique.valnet.main;

import fr.esiea.unique.valnet.engine.Game;

public class Main {

	public static void main(String[] args) {
		Game game = new Game();
		game.gameInitialization();
		return;
	}

}
