package fr.esiea.unique.valnet.engine;


import java.util.Scanner;

import fr.esiea.unique.valnet.dictionary.Dictionary;
import fr.esiea.unique.valnet.objects.CommonPot;
import fr.esiea.unique.valnet.objects.LetterOccurrenceFrequency;
import fr.esiea.unique.valnet.objects.Player;

public class Game {
	public Dictionary dictionary;
	public CommonPot pot;
	Scanner scanner;
	private LetterOccurrenceFrequency letterOccurrenceFrequency;

	Player player1 = new Player();
	Player player2 = new Player();
	
	public Game(){
		dictionary = new Dictionary();
		pot = new CommonPot();
		scanner = new Scanner(System.in);
		letterOccurrenceFrequency = new LetterOccurrenceFrequency(dictionary.getLetterFrequencyMap());
	}
	
	public void gameInitialization()
	{
		char l1 = player1.pickLetter(letterOccurrenceFrequency);
		pot.addLetter(l1);
		char l2 = player2.pickLetter(letterOccurrenceFrequency);
		pot.addLetter(l2);
		
		System.out.println(pot);
		
		int i = whoStart(l1, l2);
		
		if (i == 1) { //Player 1
			gameEngine(player1, player2);
		}
		else { //Player 2
			gameEngine(player2, player1);
		}
	}
	
	public int whoStart(char p1, char p2)
	{
		if (p1 == p2) {
			System.out.println("Players picked the same letter. Player 1 starts first.\n");
		} 
		else if (p1 < p2) {
			System.out.println("Player 1 starts.\n");
		}
		else {
			System.out.println("Player 2 starts.\n");
			return 2;
		}
		return 1;
		
	}
	
	public boolean isValidWord(String word){
	
		if(word.isEmpty() || word.equals("pass")){
			System.out.println("You have missed your turn");
			return false;
		}else if(!dictionary.isWord(word)){
			System.out.println("Created word doesn't exist in game's dictionary");
			return false;
		}else if(!pot.contains(word)){
			if(player1.isWordPossibleWithPot(pot, word)){
			}else if(player2.isWordPossibleWithPot(pot, word)){
			}else{
				System.out.println("Use only letters from common pot to form a word ");
				return false;
			}
		}
		return true;
	}
	
	public void addCompoundWord(String word, Player player){
		for(String compoundWord:word.split("-")){
			player.addWord(compoundWord);
			pot.deleteWord(compoundWord);
		}
	}
	public void gameEngine(Player selectedPlayer, Player waitingPlayer) {
	
		String createdWord = null;
		Boolean valid = true;
		
		System.out.println("");
		System.out.println("----------------------------------------------------");
		System.out.println(selectedPlayer+ " is playing");
		System.out.println("----------------------------------------------------");
		System.out.println(selectedPlayer+ " picks two letters");
		
		for(int i=0; i <=1; i++){
			char letter = selectedPlayer.pickLetter(letterOccurrenceFrequency);
			pot.addLetter(letter);
		}
		
		do{
			System.out.println("Available letters: " + pot);
			System.out.println(selectedPlayer+" words"+ ": "+selectedPlayer.words);
			System.out.println(waitingPlayer+" words" + ": "+waitingPlayer.words);
			System.out.println("To pass your turn, enter <pass> ");
			System.out.println("Form a word: ");
			createdWord = scanner.nextLine();
			valid = isValidWord(createdWord);
			if(valid){
				System.out.println("Well done! The word " + "<"+ createdWord +">"+ " exists! ");
				if(createdWord.contains("-")){
					addCompoundWord(createdWord,selectedPlayer);
				}else if(selectedPlayer.isWordPossibleWithPot(pot, createdWord)){
					String word = selectedPlayer.stealFirstPossibleWordWithPot(pot, createdWord);
					selectedPlayer.addWord(createdWord);
					System.out.println(selectedPlayer+" re-used the word <"+word+"> of his list ");
				}else if(waitingPlayer.isWordPossibleWithPot(pot, createdWord)){
					String word = waitingPlayer.stealFirstPossibleWordWithPot(pot, createdWord);
					selectedPlayer.addWord(createdWord);
					System.out.println(selectedPlayer+" stole the word <"+ word +"> of "+waitingPlayer+"'s list");
				}else{
					selectedPlayer.addWord(createdWord);
					pot.deleteWord(createdWord);
				}
				System.out.println(selectedPlayer+" picked a new letter ");
				char letter = selectedPlayer.pickLetter(letterOccurrenceFrequency);
				pot.addLetter(letter);
			}
		}while(valid);
		
		//End of game
		if(selectedPlayer.words.size() >= 10){
			System.out.println(selectedPlayer + " won!");
		} else //End of a turn
		{
			gameEngine(waitingPlayer,selectedPlayer);
		}
		
	}
}
