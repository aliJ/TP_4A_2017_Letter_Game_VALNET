# Jeu de lettres

- Auteur : Alina Valnet TCSE3


## Fonctionnalitées du jeu 
- Le jeu LetterGame comporte les fonctionnalités suivantes:
  - Joueur à plusieurs
       - Le jeu est conçu de sorte à pouvoir joueur à plusieurs. La classe "Player" permet de définir un joueur. 
         On attribue à chaque joueur un id pour pouvoir le différencier des autres joueurs. 
  - Tirer une lettre aléatoire 
       - Chaque joueur commence par tirer une lettre aléatoire d'un sac. La classe "LetterOccurrenceFrequency" permet d'avoir le 
         nombre d'occurence de chaque lettre du dictionnaire utilisé pour permettre un tirage de lettres qui respecte les mêmes proportions. 
         La classe "CommonPot" permet d'ajouter les lettres tirées au pot common. Le moteur de jeu "Game" permet d'afficher les lettres du pot 
         commun en console.
  - Déterminer le joueur qui commence. 
       - La classe "Game" possède la méthode whoStart(char player1, char player2) qui détermine quel joueur commence 
         en fonction de la lettre qu'il a tiré au début du jeu. Le joueur qui a tiré la plus petite lettre dans l'alphabet commence.
  - Faire un mot
       - Les classes Dictionary, CommonPot, Player et Game sont utilisées pour réaliser cette fonctionnalité. La classe Dictionnary permet de 
         vérifier si le mot fait partie du dictionnaire. La classe CommonPot permet de supprimer les lettres du pot commun que le joueur utilise pour faire un mot.
         Cette classe permet également de vérifier que le mot fait utilise uniquement les lettres du pot commun. La classe Player gère les cas pour piquer les
         d'un adversaire et rallonger un de ses propres mots. La classe Game permet de vérifier si un mot est valide et s'il est composé ou simple.
  - Rejouer, Passer son tour,Fin de jeu
       - Ces fonctionnalités sont gérées par la méthode gameEngine(Player selectedPlayer, Player waitingPlayer) de la classe Game qui constitue le moteur de jeu.
         Le premier joueur ayant 10 mots gagne la partie.
      

## Architecture du jeu

## Classe Dictionary

| Methodes | Description           | 
| :----- |:-------------| 
HashSet<String> File() | Permet d'effectuer une recherche arborescente d'un mot dans le dictioannire |
boolean isWord(String testedWord) | Teste si le mot fait par un joueur fait ou non partie du dictionnaire |
Map<Character, Integer> getLetterFrequencyMap() | Donne le nombre d'occurences de chaque lettre dans le dictionnaire |

## Classe LetterOccurrenceFrequency

| Methodes | Description           | 
| :----- |:-------------| 
Character roll() | Tirer aléatoirement des lettres qui sont représentatives de la distribution des lettres dans la langue française |

## Classe CommonPot

| Methodes | Description           | 
| :----- |:-------------| 
void addLetter(Character letter) | Permet d'ajouter une lettre tirée par le joueur au pot commun |
void deleteLetter(Character letter) | Permet de supprimer une lettre du pot commun |
void deleteWord(String word) | Permet de supprimer les lettres utilisées pour faire un mot du pot commun |
boolean contains(String word) | Permet de vérifier si le joueur utilise uniquement les lettres du pot commun pour faire un mot |
String toString() | Permet d'afficher le pot commun | 

## Classe Player

| Methodes | Description           | 
| :----- |:-------------| 
char pickLetter(LetterOccurrenceFrequency letterOccurrenceFrequency) | Permet de tirer une lettre aléatoire d'un sac |
void addWord(String word) | Permet d'ajouter un mot à une liste de mots |
deleteWord(String word) | Permet de supprimer un mot d'une liste de mots |
ArrayList<String> getWordsOfArrayList(String word) | Permet de récupérer les mots d'un joueur |
String getFirstPossibleWordWithPot(CommonPot pot, String word, boolean stealWord) | Permet de récupérer le premier mot de la liste d'un joueur qui correspond à un mot fait. Le boolean stealWord est true quand on veut piquer le mot récupéré|
boolean isWordPossibleWithPot(CommonPot pot, String word) | Vérifie si un mot d'une liste de mots correspond à un mot fait à partir des lettres du pot commun |
String toString() | Permet d'afficher l'objet Player |

## Classe Game

| Methodes | Description           | 
| :----- |:-------------| 
void gameInitialization() | Permet d'initialiser le jeu |
int whoStart(char p1, char p2) | Permet de déterminer quel joueur commence. Retourne l'id du joueur qui commence |
boolean isValidWord(String word) | Permet de vérifier si un mot fait est valide |
void addCompoundWord(String word, Player player) | Traite le cas des mots composés. Ajoute deux mots dans la liste de mots du joueur et supprime les lettres utilisées du pot commun |
void gameEngine(Player selectedPlayer, Player waitingPlayer) | Moteur du jeu. Tour à tour | 

## Comment compiler et exécuter le jeu en ligne de commande avec Maven

Pour compiler le jeu avec Maven:
```
$ cd Letter_Game_VALNET
$ mvn compile
```
Pour exécuter le jeu avec Maven: 
```
$ mvn exec:java  -Dexec.mainClass="fr.esiea.unique.valnet.main.Main"
```
